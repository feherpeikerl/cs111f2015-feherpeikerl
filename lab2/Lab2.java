//******************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Name - Louis James Feher-Peiker
// CMPSC 111 Fall 2015
// Lab #2 
// Date: September 09, MMXV
//
// Purpose: to compute and print the calculations of 2-bit binary functions in addition, subtraction, multiplication, and division.
//******************************
import java.util.Date; // September 09, MMXV

public class Lab2
{

      //--------------------
// main mehthod: program execution begins here
      //--------------------
      public static void main(String[] args)
    {
      // Label output with name and date:
     System.out.println("Louis James Feher-Peiker\nLab #2\n" + new Date() + "\n");

      // Variable Dictionary:
int NumberZero = 00;  // Value-Zero 
int NumberOne = 01;   // Value-One 
int NumberTwo = 02;   // Value-Two 
int NumberThree = 03; // Value-Three
int Value;            // Output Number
// Compute values:
Value = NumberOne + NumberTwo;
Value = NumberThree - NumberZero;
Value = NumberTwo * NumberTwo;

System.out.println("01 + 02 = 03");
System.out.println("03 - 00 = 03");
System.out.println("01 * 03 = 03");


}

}

