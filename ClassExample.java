//******************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Name - Louis James Feher-Peiker
// CMPSC 111 Fall 2015
// In-Class Assignment 
// Date: September 09, MMXV
//
// Purpose: to produce a java program that utilizes int, double, float, boolean, char, and String variables.
//******************************
import java.util.Date; // September 09, MMXV

public JavaProgram
{
      //--------------------
// main mehthod: program execution begins here
      //--------------------
      public static void main(String[] args)
    {
      // Label output with name and date:
      System.out.println("Your Name\nLab #\n" + new Date() + "\n");


      // Variable Dictionary:
int NumberThree = 03;  // Value-Three
double PiDecimal = 3.14;  // Value-Pi
float PiDouble = 6.28f;  // Value-Pi*2
final boolean visible = true;  // Truth
char SingleAlpha = 7;  // Arbitrary Alphabetic Standard
String username = feherpeikerl;  // Username 

    // compute value
   }
}

