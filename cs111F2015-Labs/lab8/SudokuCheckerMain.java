//******************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Name - Louis James Feher-Peiker
// CMPSC 111 Fall 2015
// Lab #
// Date: October 21, MMXV
//
// Purpose:
//******************************
import java.util.Date; // September 09, MMXV

public class SudokuCheckerMain
{

      //--------------------
// main method: program execution begins here
      //--------------------
    public static void main ( String args [] )
    {
      // Label output with name and date:
     System.out.println("Louis James Feher-Peiker\nLab #8\n" + new Date() + "\n");

     SudokuChecker checker = new SudokuChecker();
     // TODO: output the required welcome message
     System.out.println("Welcome to the Sudoku Checker!");
     System.out.println();
     System.out.println("This program checks simple 4x4 Sudoku grids for correctness.");
     System.out.println("Each column, row, and 2x2 region contains the numbers 1 through 4 only once.");
     System.out.println();
     System.out.println("TO check yout Sudoku, enter your board one row at a time,");
     System.out.println("with each digit separated by a space. Hit ENTER at the end of each row.");
   // checker.getGrid();
    //checker.checkGrid();


    }
}
