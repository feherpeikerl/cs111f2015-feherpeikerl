//==========================================
// CMPSC 111 Fall 2015
// Lab #4 
// Date: September 16, MMXV
//
// Purpose: To paint a pretty picture in java using RGB colour coding.
//==========================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Louis James Feher-Peiker ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(700, 500);
  }
}

