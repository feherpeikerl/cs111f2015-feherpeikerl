//=================================================
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Louis James Feher-Peiker
// cmpsc 111 fall 2015
// Lab #4 
// date: september 22, MMXV
// purpose: to paint a pretty picture in java using rgb colour coding and other java methods and figures.
//=================================================
import java.awt.*;
import javax.swing.japplet;

public class Lab4 extends japplet
{
  //-------------------------------------------------
  // use graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(graphics page)
  {
     final int width = 700;
     final int height = 500;
     Color cornflowerblue = new Color(100,149,237);
     Color sand = new Color(250,197,136);
     Color cactusgreen = new Color(46,121,46);



//fill all background cornflowerblue
     page.setColor(cornflowerblue);
     page.fillRect(0,0,WIDTH,HEIGHT);

//create sand for the ground
     page.setColor(sand);
     page.fillRect(0,325,WIDTH,HEIGHT);

//create sun in top left corner
     page.setColor(Color.yellow);
     page.fillOval(0,0,100,100);


//create Sun Rays

     page.setColor(Color.yellow);
     page.drawLine(50,50,50,150); //6 Oclock
     page.drawLine(50,50,125,125); //4/5 Oclock
     page.drawLine(50,50,150,50); //3 Oclock
     page.drawLine(50,50,125,0); //1/2 Oclock
     page.drawLine(50,50,0,0); //10/11 Oclock
     page.drawLine(50,50,0,125); //7/8 Oclock

 
    

//create cactus  above x,350
     page.setColor(cactusgreen);
     page.fillOval(440,205,40,250); //Body
     page.fillOval(445,275,100,25); //Right Arm H
     page.fillOval(528,195,20,100); //Right Arm V
     page.fillOval(380,310,100,25); //Left Arm H
     page.fillOval(376,225,20,100); //Left Arm V
     page.setColor(Color.black);
     page.fillOval(410,312,20,20); //Owl's Burrow




//create billowy desert clouds
     page.setColor(Color.white);
     page.fillOval(130,40,500,40);
     page.fillOval(165,50,175,50);
     page.fillOval(190,30,210,30);

     
//create bull skull
     page.setColor(Color.white);
     page.fillOval(135,350,55,90); //Skull
     page.fillArc(155,350,200,23,190,45); //Right Horn
     page.fillArc(60,360,200,23,100,50); //Left Horn
     page.setColor(Color.black);
     page.fillOval(143,365,15,20); //Eye Left
     page.fillOval(168,365,15,20); //Eye Right
     page.fillOval(164,395,05,30); //Nostril Right
     page.fillOval(159,395,05,30); //Nostril Left









  }
}
