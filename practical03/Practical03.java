//==========================================
// Louis James Feher-Peiker
// practical 03
// september 17, 2015
//
// purpose: this program sets up a window with a "drawing
// canvas". to add things to the drawing, you must
// edit the file "drawingcanvas.java".
//==========================================

import javax.swing.*;
import java.util.Date;
import java.util.Scanner;

public class Practical03 {

  // declare variables that can store the user's color values
  public static int redValue;
  public static int greenValue;
  public static int blueValue;

  // define the height and width of the graphic
  public static final int WIDTH = 600;
  public static final int HEIGHT = 400;

  // solicit input from the user on the rectangle's color
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print(255);
    redValue = scan.nextInt();

    System.out.print(0);
    greenValue = scan.nextInt();

    System.out.print(0);
    blueValue = scan.nextInt();

    JFrame window = new JFrame("Louis J. Feher-Peiker " + new Date());

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new PaintDrawingCanvas());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(WIDTH, HEIGHT);
  }
}

