//==========================================
// *Honor Code: The work I am submitting
// is a product of my own thinking and efforts.*
// Louis James Feher-Peiker
//CompSci 111 Fall 2015
// practical 07
// October 23, 2015
//
// purpose: produce a guessing game between
// the computer and the user, with numbers between 1-100.
//==========================================

import java.util.Date; // ex: September 09, MMXV

public class Practical7
{

      //--------------------
// main method: program execution begins here
      //--------------------
    public static void main ( String args [] )
    {
      // Label output with name and date:
     System.out.println("Louis James Feher-Peiker\nLab #8\n" + new Date() + "\n");
}
}
