//==========================================
// Louis James Feher-Peiker
// practical 06
// October 23, 2015
//
// purpose: this produce a second Octopus in juxtaposition to Ocky the Octopus; enter Marv.//==========================================
//==========================================

import java.util.Date;

public class Practical6 {

    public static void main(String[] args) {

        System.out.println("Louis J. Feher-Peiker\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Octopus Marv;           // a BETTER octopus
        Utensil spat;           // a kitchen utensil
        Utensil oyst;           // an Oyster Knife: Marv's favorite utensil

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        oyst = new Utensil("Oyster Knife"); // create an Oyster Knife
        oyst.setColor("black");             // set color of Oyster Knife
        oyst.setCost(22.95);                // price of Oyster Knife

        Marv = new Octopus("Marv");         // created Octopus & Name
        Marv.setAge(27);                    // set Octopus Age
        Marv.setWeight(125);                // set Octopus Weight
        Marv.setUtensil(oyst);              // set favorite Utensil



        ocky = new Octopus("Ocky");    // create and name the octopus
        ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        System.out.println("Using 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

//***Below is Marv info to output and for pdf formating
System.out.println("Using 'get' methods:");
System.out.println(Marv.getName() + " weighs " + Marv.getWeight()
        + " pounds\n" + "and is " + Marv.getAge()
        + " years old. His favorite utensil is a "
        + Marv.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

//***Below is Marv info to output and for pdf formating
        System.out.println(Marv.getName() + "'s " + Marv.getUtensil() + " costs $"
                + Marv.getUtensil().getCost());
        System.out.println("Utensil's color: " + oyst.getColor());


        // automatically produce a visualization of the octopus
        LJV.Context dataVisualizationContext = LJV.getDefaultContext();
        dataVisualizationContext.outputFormat = "pdf";
        dataVisualizationContext.ignorePrivateFields = false;
        LJV.drawGraph( dataVisualizationContext, ocky, "ocky-before.pdf" );

        //***Below is Marv info to output and for pdf formating
         LJV.Context dataVisualizationContext2 = LJV.getDefaultContext();
        dataVisualizationContext.outputFormat = "pdf";
        dataVisualizationContext.ignorePrivateFields = false;
        LJV.drawGraph( dataVisualizationContext2, Marv, "Marv-before.pdf" );


        // Use methods to change some values:
        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

 //***Below is Marv info to output and for pdf formating
        Marv.setAge(35);
        Marv.setWeight(182);
        oyst.setCost(17.38);
        oyst.setColor("Mother of Pearl");

        System.out.println("\nUsing 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

          System.out.println("\nUsing 'set' methods:");
          System.out.println(Marv.getName() + "'s new age: " + Marv.getAge());
          System.out.println(Marv.getName() + "'s new weight: " + Marv.getWeight());
          System.out.println("Utensil's new cost: $" + oyst.getCost());
          System.out.println("Utensil's new color: " + oyst.getColor());


        LJV.drawGraph( dataVisualizationContext, ocky, "ocky-after.pdf" );

        LJV.drawGraph( dataVisualizationContext2, Marv, "Marv-after.pdf" );


    }

}
